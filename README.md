# Proboards Website Generator

A  quick way to visualise the data obtained by my [proboards-saver](https://gitlab.com/thallian/proboards-saver) tool.  

Python3 with Jinja2 is needed.

## Usage

    generator.py [-h] [--data DATA] [--static STATIC] [--out OUT]

    build a static website out of a proboard json dump

    optional arguments:
      -h, --help       show this help message and exit
      --data DATA      board data (json file)
      --static STATIC  path to the static files (images, attachments)
      --out OUT        path where the website gets rendered to
